title:      "CheckList 02"
icon:       fa-pencil
navigation: []
internal:
external:
body:       |
    ## Overview

    The second quiz will cover the following topics:

    1. Conditional Execution

    2. Iteration

    The quiz will consist of definitions, short answers, translation, code
    evaluation, debugging, and programming sections.

    Below are examples of the type of questions that may be asked on the quiz.
    It is meant to be representative, rather than exhaustive (ie. there may be
    questions that show up on the quiz that are not shown below).

    ## Definitions

    Briefly **define** the following terms.

    1. **Boolean Expression**

    2. **Exception**

    3. **Short Circuit Evaluation**

    4. **Infinite Loop**

    ## Short Answer

    1. What is the difference between a while loop and a for loop? Give two
    examples where we would prefer one over the other.

    ## Translation

    1. Convert the following Python code to use a `while` loop instead of a
    `for` loop:

            :::python
            numbers = [5, 4, 7, 0, 1]
            count   = 0

            for number in numbers:
                if number:
                    break
                count += 1

    ## Code Evaluation

    1. What is the result of the following Python code:

            :::python
            for i in range(0, 10):
                if i < 4:
                    continue
                elif i == 7:
                    break
                else:
                    print i
            print i

    ## Debugging

    1. The Python code below is suppose to count all of the even numbers
    between `1` and `100` (inclusive) that are also a multiple of `5`, but
    it contains some errors. Identify and fix the errors:

            :::python
            while i < 100:
                if i % 2 or i % 5:
                    total + 1
            print total

    ## Programming

    1. Given a list of random integers, `numbers`, write Python code that
    computes the sum of all the numbers between `a` and `b` (inclusive) and
    prints the resulting total.

    2. Write Python code that prints 'Yeah' if a number `n` is between the values `a` and `b` (inclusive). Otherwise, print 'Nope'.

    3. Write Python code that generates `n` random numbers (between `0` and `9`), counts the number of odd and even numbers, and then prints out the totals.

    4. Write Python code that computes the list of the even numbers between `0` and `100` that are also a multiple of `7`.

    5. Write Python code that simulates rolling a die until we a get a `6` and
    reports how many rolls it took.
