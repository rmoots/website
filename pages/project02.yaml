title:      "Project 02: Web Programming"
icon:       fa-columns
navigation: []
internal:
external:
body:       |
    ## Overview

    The goal of this project is to build a simple application using the
    [Python] programming language and the [Tornado] web framework.

    To record your work, create the necessary [Python] scripts, [HTML] files,
    and [Jupyter] Notebooks in the `project02` folder.

    This Project assignment is due **at the beginning of class Thursday,
    December 8, 2016** and is to be done **individually** or in **pairs**.

    <div class="alert alert-info">
      <h4><i class="fa fa-users"></i> Partners</h4>

      Both members of each pair should submit a Notebook.  Be sure to identify
      your partner at the top of the Notebook.
    </div>

    [Python]:       http://python.org/
    [Jupyter]:      https://jupyter.org/
    [Markdown]:     http://daringfireball.net/projects/markdown/
    [Bitbucket]:    https://bitbucket.org/
    [Git]:          https://git-scm.com/

    [HTML]:         https://developer.mozilla.org/en-US/docs/Web/HTML/
    [Tornado]:      http://www.tornadoweb.org/en/stable/

    ## Overview

    For this activity, you are to build a web application that meets the
    requirements below.

    Here are some possible web applications ideas:

      1. Search engine for local files
      2. Search engine for a local set of bookmarks/favorite links
      3. Guest book / Message board
      4. Image gallery
      5. Todo / Organizer
      6. Calculator / Math Formula Processor
      7. Survey
      8. Personality tester

    There are all sorts of things you can do!  If you have a question about a
    possible idea, please discuss it with the instructor.

    ### Client-Side Requirements

    The [HTML] or client-side code produced by the web application must meet
    the following requirements:

      1. Web page displays at least one link to the internal web application.
      2. Web page displays at least one link to an external website.
      3. Web page displays at least one image.
      4. Web page displays a list or table.

    ### Server-Side Requirements

    The [Tornado] or server-side code must meet the following requirements:

      1. Web application processes user input via a form.
      2. Web application uses data read from a local file.
      3. Web application uses a loop of some sort.
      4. Web application uses a data structure such as a `list` or `dict`.

    ### Extra Credit

    You may choose to do the following to earn extra credit:

      1. Utilize audio or video.
      2. Utilize external data from another web service (e.g [JSON] via [Requests]).
      3. Utilize client-side tools such as [Bootstrap] or [Font Awesome] to
      improve appearance.

    [JSON]:         http://www.json.org/
    [Requests]:     http://docs.python-requests.org/en/latest/
    [Bootstrap]:    http://getbootstrap.com/
    [Font Awesome]: http://fortawesome.github.io/Font-Awesome/icons/
    
    ## Submission

    To submit your work, follow the same directions for [Notebook 01], except
    store all the files related to this assignment in the **project02** folder.

    Your group will be required to present your web application in class on
    **Thursday, December 8, 2016**.  Each presentation should be no more than
    2 or 3 minutes and should:

      1. Describe the purpose and design of the web application.
      2. Discuss the role of each member in the group.
      3. Demonstrate the web application.
      4. Analyze the result of the project (what works, what doesn't work, etc.).

    [Notebook 01]: notebook01.html
