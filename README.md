CDT 30010 Fall 2016
===================

This is the source code for the CDT 30010 Elements of Computing I (Fall 2016)
[course website](http://www3.nd.edu/~pbui/teaching/cdt.30010.fa16/).
